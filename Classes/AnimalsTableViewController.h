//
//  UIAnimalsTableViewController.h
//  Animal
//
//  Created by Ngendo Muhayimana on 11/08/09.
//  .
//

#import <UIKit/UIKit.h>


@interface AnimalsTableViewController : UITableViewController {
	NSArray * lists;
	UIImage * icon;
}

@property (nonatomic,strong) NSArray* lists;
@property (nonatomic,strong) UIImage* icon;

@end
