//
//  AnimalAppDelegate.h
//  Animal
//
//  Created by Ngendo Muhayimana on 11/08/09.
//  Copyright Executable Design Inc. 2009. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnimalAppDelegate : NSObject <UIApplicationDelegate, UITabBarControllerDelegate> {
    UIWindow *window;
    UITabBarController *tabBarController;
}

@property (nonatomic, strong) IBOutlet UIWindow *window;
@property (nonatomic, strong) IBOutlet UITabBarController *tabBarController;

@end
