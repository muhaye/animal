//
//  UIAnimalViewController.h
//  Animal
//
//  Created by Ngendo Muhayimana on 24/08/09.
//  .
//

#import <UIKit/UIKit.h>


@interface AnimalViewController : UIViewController {
	IBOutlet UILabel* label;
	IBOutlet UIImageView* imageView;

}

@property (nonatomic,strong) IBOutlet UILabel* label;
@property (nonatomic,strong) IBOutlet UIImageView* imageView;

@end
