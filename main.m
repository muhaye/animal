//
//  main.m
//  Animal
//
//  Created by Ngendo Muhayimana on 11/08/09.
//  Copyright Executable Design Inc. 2009. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, nil);
        return retVal;
    }
}

